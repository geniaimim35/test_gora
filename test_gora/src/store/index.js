import Vue from 'vue'
import Vuex from 'vuex'
import auth from './auth'


Vue.use(Vuex);


export default new Vuex.Store({
    state:{
        email:null,
        password:null,
        //деревянные значения
    },
    mutations:{
        //изминения стейта синхронные аперации
    },
    actions:{
        //асинхронные операции прописы аксиос эйчи
    },
    modules:{
        auth
    },
    getters:{
        //компьютед свойства как в компоненте можно меп стейт использовать или вис стор map.state map.getters
    }
})