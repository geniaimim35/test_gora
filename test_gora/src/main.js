import Vue from 'vue'
import App from './App.vue'
import '@/assets/style/main.scss'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import router from './router'
import store from './store'

import firebase from 'firebase/app' 
import 'firebase/auth';
import 'firebase/database';

Vue.config.productionTip = false

Vue.use(ElementUI);



firebase.initializeApp({
  apiKey: "AIzaSyAHzratISoapz0Kmfy-2LY826yiVIyCjfA",
  authDomain: "testgora-code.firebaseapp.com",
  databaseURL: "https://testgora-code.firebaseio.com",
  projectId: "testgora-code",
  storageBucket: "testgora-code.appspot.com",
  messagingSenderId: "964811651821",
  appId: "1:964811651821:web:b1d327dd91263e38e7eae4",
  measurementId: "G-5JG1QZX70W"
})

let app

firebase.auth().onAuthStateChanged(() => {
  if(!app){
    app = new Vue({
      router,
      store,
      render: h => h(App),
    }).$mount('#app')
  }
})


