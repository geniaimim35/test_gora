import Vue from 'vue'
import Router from 'vue-router'
import Login from './pages/Login.vue'
import Content from './pages/Content.vue'
import Register from './pages/Register.vue'



Vue.use(Router)

const routes = [
    {
        path: '/login',
        name: 'login',
        component: Login
    },
    {
        path: '/register',
        name: 'register',
        component: Register
    },
    {
        path: '/content',
        name: 'content',
        component: Content
    },
]
const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})
export default router